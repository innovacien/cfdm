const home = require('../controllers/home');
const participantes = require('../controllers/participantes');
const convocatorias = require('../controllers/convocatorias');
const perfil = require('../controllers/perfil');
const admin = require('../controllers/admin');
const colaborar = require('../controllers/colaborar');
const login = require('../controllers/login');
const expressValidator = require('express-validator');

const express = require('express'),
    router = express.Router(),
    path = require('path');


module.exports = (app) => {
    router.use(expressValidator());
    router.get('/', home.index);
    router.get('/participantes', participantes.main);
    router.get('/convocatorias', convocatorias.main);
    router.get('/convocatorias/:individual', convocatorias.individual);
    router.get('/participantes/:identificador', perfil.main);
    router.get('/login', login.main);
    router.get('/colaborar', colaborar.main);
    router.get('/admin', admin.main);
    router.post('/admin/adduser', admin.addUser);
    router.post('/admin/addconvocatoria', admin.addConvocatoria)
    app.use(router);
};