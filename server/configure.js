const path = require('path'),
	routes = require('./routes'),
	express = require('express'),
	exphbs = require('express-handlebars'),
	bodyParser = require('body-parser');


//



module.exports = (app) =>{
	
	app.use('/public/', express.static(path.join(__dirname, '../public')));
	app.use(bodyParser.urlencoded({'extended': true}));
	app.use(bodyParser.json());
		routes(app);

		app.engine('hbs', exphbs.create({
			defaultLayout: 'main',
			extname: '.hbs',
			layoutsDir: app.get('views') + '/layouts',
        	partialsDir: [app.get('views') + '/partials'],
			helpers: {
				timeago: (timestamp) => {
					return moment(timestamp).startOf('minute').fromNow();
				}
			}
		}).engine);
		routes(app);//moving the routes to routes folder.

		app.set('view engine', '.hbs');
		//app.set('views', path.join(__dirname, 'views'));

		return app;
	};
