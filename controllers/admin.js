const users = require('../model/user');
const convocatorias = require('../model/convocatoria');
const usuarios = require('../model/user');

module.exports = {
    main(req, res) {
        convocatorias.find({}).exec((err, convocatoria) => {
            if (err) throw err;
            usuarios.find({}).exec((err, usuario) => {
                res.render('admin', { convocatoria: convocatoria, usuario: usuario });
            })

        })



    },
    addUser(req, res) {
        const name = req.body.addname;
        const lastname = req.body.addlastname;
        const birthdate = req.body.addbirthdate;
        const email = req.body.addemail;
        const ubicacion = req.body.addlocation;
        //const img = req.body.addimg;
        const skills = req.body.addskills;
        var inputStudent = new users({
            nombre: name,
            apellido: lastname,
            edad: birthdate,
            habilidades: skills,
            contacto: {
                correo: email,
                location: ubicacion
            }
        });
        inputStudent.save()
        res.redirect('/admin');
    },
    addConvocatoria(req, res) {
        const nombre = req.body.convocatorianame;
        console.log("1" + nombre)
        const encargado = req.body.convocatoriaencargado;
        console.log("2" + encargado)
        const correoenc = req.body.convocatoriacorreo;
        console.log("3" + correoenc)
        const convfecha = req.body.convocatoriafecha;
        console.log("4" + convfecha)
        const convubi = req.body.convocatoriaubicacion;
        console.log("5" + convubi)

        var newconvo = new convocatorias({
            nombre: nombre,
            descripcion: 'test description to replace',
            imagen: 's',
            contacto: {
                encargado: encargado,
                correo: correoenc,
                fecha: convfecha,
                ubicacion: convubi
            }
        })
        newconvo.save()
        res.redirect('/admin')


    }
}