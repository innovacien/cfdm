const usuarios = require('../model/user');

module.exports = {
    main(req, res) {
        usuarios.find({}).exec((err, usuario) => {
            if (err) throw err;
            res.render('participantes', { usuario: usuario })
        })

    }
}