const convocatorias = require('../model/convocatoria');

module.exports = {
    main(req, res) {
        convocatorias.find({}).exec((err, convocatoria) => {
            res.render('convocatorias', { convocatoria: convocatoria })
        })
    },
    individual(req, res) {
        res.render('convocatoria-individual');
    }
}