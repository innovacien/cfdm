
/*FROALA*/
$(function () {
    $('textarea#froala-editor').froalaEditor({
        heightMin: 100,
        heightMax: 200
    })
});

$(document).ready(function () {
    //SMOOTH SCROLL HOME
    $("a").on('click', function (event) {
        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 800, function () {
                window.location.hash = hash;
            });
        }
    });

    $('#moreinfo').on('click', function (event) {
        alert("CLICKED");
        $('.convocatoria-modal-wrap').css('display', 'block');
    })
});

/*ADMIN PANEL*/
$('#add').on('click', function () {
    $('#add-student-modal-wrap').fadeIn('slow');
})

$('#addclose').on('click', function () {
    $('#add-student-modal-wrap').fadeOut('fast');
})

$('#addconvocatoria').on('click', function () {
    $('#add-convocatoria-modal-wrap').fadeIn('slow');
})

$('#closeconvocatoria').on('click', function () {
    $('#add-convocatoria-modal-wrap').fadeOut('fast');
})



