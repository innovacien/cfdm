const express = require('express');
const config = require('./server/configure');
const mongoose = require('mongoose');

var app = express()



app.set('port', process.env.PORT || 3300);
app.set('Views', `${__dirname}/Views`);

app = config(app);

mongoose.connect('mongodb://cfdm:cfdmadmin1@ds221292.mlab.com:21292/cfdm');

mongoose.connection.on('open', () => {
  console.log('Mongoose connected');
});

app.listen(app.get('port'), () => {
  console.log(`Server up: http://localhost:${app.get('port')}`);
});