const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BDusuario = new Schema({
    nombre: { type: String },
    apellido: { type: String },
    edad: { type: String },
    habilidades: { type: Array },
    descripcion: { type: String },
    contacto: {
        social: { type: Array },
        correo: { type: String },
        ubicacion: { type: String }
    }

})

var Usuario = module.exports = mongoose.model('usuarios', BDusuario);

//email: {
    //type: String,
    //unique: true,
   // required: true,
    //trim: true
//},
//username: {
  //  type: String,
    //unique: true,
    //required: true,
    //trim: true
//},
//password: {
  //  type: String,
  //  required: true,
//},
//passwordConf: {
    //type: String,
    //required: true,
//}