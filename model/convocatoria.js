const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var BDconvocatoria = new Schema({
    nombre: { type: String },
    descripcion: { type: String },
    imagen: { type: String },
    contacto: {
        encargado: { type: String },
        correo: { type: String },
        fecha: { type: String },
        ubicacion: { type: String }
    }
})

var Convocatoria = module.exports = mongoose.model('convocatorias', BDconvocatoria);