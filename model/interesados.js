const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var BDinteresados = {
    nombre: { type: String },
    apellido: { type: String },
    edad: { type: String },
    contacto: {
        correo: { type: String }
    },
    interes: { type: Array },
    ocupacion: { type: String }
}

var Interesado = module.exports = mongoose.model('interesados', BDinteresados)