const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var BDcolaborador = {
    nombre: { type: String },
    apellido: { type: String },
    edad: { type: String },
    profesion: { type: String },
    mensaje: { type: String },
    contacto: {
        telefono: { type: String },
        correo: { type: String }
    },
    comuna: { type: String },
    ciudad: { type: String }
}

var Colaborador = module.exports = mongoose.model('colaboradores', BDcolaborador);